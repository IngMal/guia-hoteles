$(function(){
    $("[data-toogle='tooltip']").tooltip();
    $("[data-toogle='popover']").popover();
    $('.carousel').carousel({
        interval: 3000
    });

    $('#contacto').on('show.bs.modal', function (e){
        console.log('El modal se esta mostrando')

        $('contactoBtn').removeClass('btn-outline-success');
        $('contactoBtn').addClass('btn-primary');
        $('contactoBtn').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function (e){
        console.log('El modal se mostró')
    });
    $('#contacto').on('hide.bs.modal', function (e){
        console.log('El modal se esta ocultando')
    });
    $('#contacto').on('hidden.bs.modal', function (e){
        console.log('El modal se ocultó')

        $('contactoBtn').prop('disabled', false);
    });
});